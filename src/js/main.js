//accordion

document.querySelectorAll('.accordionBox__itemTitle').forEach((item) =>
    item.addEventListener('click', () => {
        const parent = item.parentNode;
        if (parent.classList.contains('accordionBox__item_open')) {
            parent.classList.remove('accordionBox__item_open');
        } else {
            document
                .querySelectorAll('.accordionBox__item')
                .forEach((child) => {
                    child.classList.remove('accordionBox__item_open');
                });
            parent.classList.toggle('accordionBox__item_open');
        }

    })
)

//accordion

//modal form

const modalForm = document.getElementById('modalForm');
const btnOpen = document.getElementById('openModal');
const close = document.getElementsByClassName('modal__formCloseIcon')[0];

btnOpen.addEventListener('click', () => {
    modalForm.classList.add('containerModal_open');
});
close.addEventListener('click', () => {
    modalForm.classList.remove('containerModal_open');
});
window.addEventListener('click', (e) => {
    if (e.target === modalForm) {
        modalForm.classList.remove('containerModal_open');
    }

});

(function () {
    'use strict'

    var forms = document.querySelectorAll('.needs-validation');

    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault();
                    event.stopPropagation();
                }

                form.classList.add('was-validated');
            }, false);
        });
})();


const inp = document.getElementById('inputName');
const counter = document.getElementById('counter');
inp.oninput = inp.onkeyp = inp.onchange = function () {
    const len = this.value.length;
    if (len > this.max) {
        this.value = this.value.substr(0, this.max);
        return false;
    }

    counter.innerHTML = 'Осталось: ' + (+this.max - len) + ' знака';

};


inp.onfocus = function () {
    counter.classList.add('counter_open');
}

inp.onblur = function () {
    counter.classList.remove('counter_open');
}

//modal form
